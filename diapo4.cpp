// my first string
#include <iostream>
#include <string>

using namespace std;

int main()
{
    string mystring;
    mystring = "this is the initial string content";
    cout << mystring << endl;
    mystring = "this is a different string content";
    cout << mystring << endl;
    return 0;
}
